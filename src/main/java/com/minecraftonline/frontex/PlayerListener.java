package com.minecraftonline.frontex;

import com.minecraftonline.frontex.dao.Note;
import com.minecraftonline.frontex.dao.Player;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.profile.GameProfile;
import org.spongepowered.api.text.Text;

public class PlayerListener {
    @Listener
    public void onClientAuth(ClientConnectionEvent.Auth event) {
        GameProfile profile = event.getProfile();
        Optional<Player> optionalPlayer = Player.findByUUID(profile.getUniqueId());

        Player player;
        // The profile has already been filled by the server
        if (optionalPlayer.isPresent()) {
            player = optionalPlayer.get();
            if (!player.getName().equals(profile.getName().get())) {
                String oldName = player.getName();
                String newName = profile.getName().get();

                player.setName(newName);

                Note nameChange = new Note(Instant.now(), player, player, Note.Type.LOG, "Name changed from " + oldName + " to " + newName);
                nameChange.save();
            }
        } else {
            player = new Player(profile.getUniqueId(), profile.getName().get(), Instant.now(), Instant.now());
        }

        player.setLastLogin(Instant.now());
        player.save();
    }

    @Listener
    public void onClientJoin(ClientConnectionEvent.Join event) {
        org.spongepowered.api.entity.living.player.Player player = event.getTargetEntity();
        List<Note> warnings = Note.findByPlayer(Player.findByUUID(player.getUniqueId()).get(),
                stream -> stream
                        .filter(note -> note.getType() == Note.Type.WARN)
                        .sorted()
                        .collect(Collectors.toList()));

        if (!warnings.isEmpty()) {
            int warnCount = warnings.size();
            String notePlural = warnCount == 1 ? "note:" : "notes:";
            player.sendMessage(Text.of("You have ", warnCount, " warning ", notePlural));
            Commands.STAFF_CHANNEL.send(Text.of(player.getName(), " has ", warnCount, " warning ", notePlural));

            warnings.stream()
                    .map(Commands::noteToText)
                    .forEachOrdered(text -> {
                        player.sendMessage(text);
                        Commands.STAFF_CHANNEL.send(text);
                    });
        }
    }
}
