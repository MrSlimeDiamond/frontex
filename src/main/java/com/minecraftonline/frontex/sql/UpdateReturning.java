package com.minecraftonline.frontex.sql;

import be.bendem.sqlstreams.SqlStream;
import be.bendem.sqlstreams.Update;
import be.bendem.sqlstreams.util.SqlConsumer;
import be.bendem.sqlstreams.util.SqlFunction;
import be.bendem.sqlstreams.util.Wrap;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class UpdateReturning implements Update {
    private final Update update;

    public UpdateReturning(SqlStream sqlStream, String sql) {
        update = sqlStream.update(conn ->
                conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS));
    }

    public <T> Stream<T> generated(SqlFunction<ResultSet, T> mapping) {
        ResultSet rs = Wrap.get(update.getStatement()::getGeneratedKeys);
        // Quick reimplementation of the logic as found in ResultSetSpliterator
        return StreamSupport.stream(
                new Spliterators.AbstractSpliterator<T>(Long.MAX_VALUE, Spliterator.ORDERED) {
                    @Override
                    public boolean tryAdvance(Consumer<? super T> action) {
                        return Wrap.get(() -> {
                            boolean hasNext;
                            if (hasNext = rs.next()) {
                                action.accept(mapping.apply(rs));
                            }
                            return hasNext;
                        });
                    }
                }, false);
    }

    @Override
    public int count() {
        return update.count();
    }

    @Override
    public long largeCount() {
        return update.largeCount();
    }

    @Override
    public UpdateReturning prepare(SqlConsumer<PreparedStatement> preparator) {
        update.prepare(preparator);
        return this;
    }

    @Override
    public UpdateReturning with(Object... params) {
        update.with(params);
        return this;
    }

    @Override
    public UpdateReturning set(int index, Object x) {
        update.set(index, x);
        return this;
    }

    @Override
    public UpdateReturning setArray(int index, Array x) {
        update.setArray(index, x);
        return this;
    }

    @Override
    public UpdateReturning setAsciiStream(int index, InputStream x) {
        update.setAsciiStream(index, x);
        return this;
    }

    @Override
    public UpdateReturning setAsciiStream(int index, InputStream x, int length) {
        update.setAsciiStream(index, x, length);
        return this;
    }

    @Override
    public UpdateReturning setAsciiStream(int index, InputStream x, long length) {
        update.setAsciiStream(index, x, length);
        return this;
    }

    @Override
    public UpdateReturning setBigDecimal(int index, BigDecimal x) {
        update.setBigDecimal(index, x);
        return this;
    }

    @Override
    public UpdateReturning setBinaryStream(int index, InputStream x) {
        update.setBinaryStream(index, x);
        return this;
    }

    @Override
    public UpdateReturning setBinaryStream(int index, InputStream x, int length) {
        update.setBinaryStream(index, x, length);
        return this;
    }

    @Override
    public UpdateReturning setBinaryStream(int index, InputStream x, long length) {
        update.setBinaryStream(index, x, length);
        return this;
    }

    @Override
    public UpdateReturning setBlob(int index, Blob x) {
        update.setBlob(index, x);
        return this;
    }

    @Override
    public UpdateReturning setBlob(int index, InputStream x) {
        update.setBlob(index, x);
        return this;
    }

    @Override
    public UpdateReturning setBlob(int index, InputStream x, long length) {
        update.setBlob(index, x, length);
        return this;
    }

    @Override
    public UpdateReturning setBoolean(int index, boolean x) {
        update.setBoolean(index, x);
        return this;
    }

    @Override
    public UpdateReturning setByte(int index, byte x) {
        update.setByte(index, x);
        return this;
    }

    @Override
    public UpdateReturning setBytes(int index, byte[] x) {
        update.setBytes(index, x);
        return this;
    }

    @Override
    public UpdateReturning setCharacterStream(int index, Reader x) {
        update.setCharacterStream(index, x);
        return this;
    }

    @Override
    public UpdateReturning setCharacterStream(int index, Reader x, int length) {
        update.setCharacterStream(index, x, length);
        return this;
    }

    @Override
    public UpdateReturning setCharacterStream(int index, Reader x, long length) {
        update.setCharacterStream(index, x, length);
        return this;
    }

    @Override
    public UpdateReturning setClob(int index, Clob x) {
        update.setClob(index, x);
        return this;
    }

    @Override
    public UpdateReturning setClob(int index, Reader x) {
        update.setClob(index, x);
        return this;
    }

    @Override
    public UpdateReturning setClob(int index, Reader x, long length) {
        update.setClob(index, x, length);
        return this;
    }

    @Override
    public UpdateReturning setDate(int index, Date x) {
        update.setDate(index, x);
        return this;
    }

    @Override
    public UpdateReturning setDate(int index, Date x, Calendar cal) {
        update.setDate(index, x, cal);
        return this;
    }

    @Override
    public UpdateReturning setDouble(int index, double x) {
        update.setDouble(index, x);
        return this;
    }

    @Override
    public UpdateReturning setFloat(int index, float x) {
        update.setFloat(index, x);
        return this;
    }

    @Override
    public UpdateReturning setInt(int index, int x) {
        update.setInt(index, x);
        return this;
    }

    @Override
    public UpdateReturning setLong(int index, long x) {
        update.setLong(index, x);
        return this;
    }

    @Override
    public UpdateReturning setNCharacterStream(int index, Reader x) {
        update.setNCharacterStream(index, x);
        return this;
    }

    @Override
    public UpdateReturning setNCharacterStream(int index, Reader x, long length) {
        update.setNCharacterStream(index, x, length);
        return this;
    }

    @Override
    public UpdateReturning setNClob(int index, NClob x) {
        update.setNClob(index, x);
        return this;
    }

    @Override
    public UpdateReturning setNClob(int index, Reader x) {
        update.setNClob(index, x);
        return this;
    }

    @Override
    public UpdateReturning setNClob(int index, Reader x, long length) {
        update.setNClob(index, x, length);
        return this;
    }

    @Override
    public UpdateReturning setNString(int index, String x) {
        update.setNString(index, x);
        return this;
    }

    @Override
    public UpdateReturning setNull(int index, int sqlType) {
        update.setNull(index, sqlType);
        return this;
    }

    @Override
    public UpdateReturning setNull(int index, int sqlType, String typeName) {
        update.setNull(index, sqlType, typeName);
        return this;
    }

    @Override
    public UpdateReturning setObject(int index, Object x) {
        update.setObject(index, x);
        return this;
    }

    @Override
    public UpdateReturning setObject(int index, Object x, int targetSqlType) {
        update.setObject(index, x, targetSqlType);
        return this;
    }

    @Override
    public UpdateReturning setObject(int index, Object x, int targetSqlType, int scaleOrLength) {
        update.setObject(index, x, targetSqlType, scaleOrLength);
        return this;
    }

    @Override
    public UpdateReturning setObject(int index, Object x, SQLType targetSqlType) {
        update.setObject(index, x, targetSqlType);
        return this;
    }

    @Override
    public UpdateReturning setObject(int index, Object x, SQLType targetSqlType, int scaleOrLength) {
        update.setObject(index, x, targetSqlType, scaleOrLength);
        return this;
    }

    @Override
    public UpdateReturning setRef(int index, Ref x) {
        update.setRef(index, x);
        return this;
    }

    @Override
    public UpdateReturning setRowId(int index, RowId x) {
        update.setRowId(index, x);
        return this;
    }

    @Override
    public UpdateReturning setShort(int index, short x) {
        update.setShort(index, x);
        return this;
    }

    @Override
    public UpdateReturning setSQLXML(int index, SQLXML x) {
        update.setSQLXML(index, x);
        return this;
    }

    @Override
    public UpdateReturning setString(int index, String x) {
        update.setString(index, x);
        return this;
    }

    @Override
    public UpdateReturning setTime(int index, Time x) {
        update.setTime(index, x);
        return this;
    }

    @Override
    public UpdateReturning setTime(int index, Time x, Calendar cal) {
        update.setTime(index, x, cal);
        return this;
    }

    @Override
    public UpdateReturning setTimestamp(int index, Timestamp x) {
        update.setTimestamp(index, x);
        return this;
    }

    @Override
    public UpdateReturning setTimestamp(int index, Timestamp x, Calendar cal) {
        update.setTimestamp(index, x, cal);
        return this;
    }

    @Override
    public UpdateReturning setURL(int index, URL x) {
        update.setURL(index, x);
        return this;
    }

    @Override
    public PreparedStatement getStatement() {
        return update.getStatement();
    }

    @Override
    public boolean execute() {
        return update.execute();
    }

    @Override
    public void close() {
        update.close();
    }
}
