/*
 * Copyright (C) 2017, 2018 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
CREATE TABLE "players" (
 "id" INTEGER NOT NULL PRIMARY KEY,
 "uuid" CHAR(36) NOT NULL UNIQUE,
 "name" VARCHAR(16) NOT NULL,
 "first_login" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 "last_login" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARSET=utf8;

CREATE TABLE "notes" (
 "id" INTEGER NOT NULL PRIMARY KEY,
 "datetime" TIMESTAMP NOT NULL,
 "player_id" INTEGER NOT NULL REFERENCES "players" ("id"),
 "author_id" INTEGER NOT NULL REFERENCES "players" ("id"),
 "type" INTEGER NOT NULL,
 "message" VARCHAR(512)
) DEFAULT CHARSET=utf8;
